import java.util.Scanner;

public class CalculatorRevise2 {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        mainMenu();
    }
    public static void mainMenu () {
        System.out.println("----------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        System.out.println("----------------------------------------");
        System.out.println("Menu ");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volume");
        System.out.println("3. Tutup Aplikasi");
        System.out.println("----------------------------------------");
        System.out.print("Pilihan : ");
        int pilihan = scanner.nextInt();

        switch (pilihan){
            case 1:
                bidang();
                break;
            case 2:
                volum();
                break;
            case 3:
                break;
            default:
                mainMenu();

        }
    }
    public static void bidang(){
        System.out.println("--------------------------------");
        System.out.println("Pilih Bidang Yang Akan dihitung");
        System.out.println("--------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("0. Kembali ke Menu Sebelumnya");
        System.out.print("Pilihan : ");
        int pilihan1 = scanner.nextInt();
        switch (pilihan1){
            case 1:
              // rumus persegi
                System.out.println("--------------------------------");
                System.out.println("Anda memilih persegi");
                System.out.println("--------------------------------");

                System.out.print("Masukkan panjang: ");
                int sisi = scanner.nextInt();

                int hasil = sisi * sisi;
                System.out.println("\nprocessing\n");
                System.out.println("Luas dari persegi adalah " + hasil);
                System.out.println("---------------------------------------");

                System.out.print("Tekan apa saja untuk kembali ke menu utama");
                scanner.next();
                mainMenu();
                break;
            case 2:
                //rumus lingkaran
                System.out.println("--------------------------------");
                System.out.println("Anda memilih Lingkaran");
                System.out.println("--------------------------------");
                System.out.println("Masukan Nilai Jari-Jari : ");

                double jari2 = scanner.nextDouble(); // inputan
                double luas = Math.PI * Math.pow(jari2,2); // rumus dengan meggunakan metode pow(pangkat) & PI(3.14)
                System.out.println("\nprocessing\n");
                System.out.println("Luas Lingkaran dari Jari-Jari = "+jari2+" Adalah "+luas);
                System.out.println("---------------------------------------");

                System.out.print("Tekan apa saja untuk kembali ke menu utama");
                scanner.next();
                mainMenu();
                break;
            case 3:
                //rumus segitiga
                System.out.println("--------------------------------");
                System.out.println("Anda memilih segitiga");
                System.out.println("--------------------------------");

                System.out.print("Masukkan alas: ");
                int alas = scanner.nextInt();

                System.out.println("Masukkan tinggi: ");
                int tinggi = scanner.nextInt();

                double luasSegitiga = 0.5 * alas * tinggi;
                System.out.println("\nprocessing\n");
                System.out.println("Luas dari segitiga adalah " + luasSegitiga);
                System.out.println("---------------------------------------");

                System.out.print("Tekan apa saja untuk kembali ke menu utama");
                scanner.next();
                mainMenu();
                break;
            case 4:
                //rumus persegi panjang
                System.out.println("--------------------------------");
                System.out.println("Anda memilih persegi panjang");
                System.out.println("--------------------------------");

                System.out.print("Masukkan panjang: ");
                int panjang = scanner.nextInt();

                System.out.println("Masukkan lebar: ");
                int lebar = scanner.nextInt();

                int luasPersegiPanjang = panjang * lebar;
                System.out.println("\nprocessing\n");
                System.out.println("Luas dari persegi panjang adalah " + luasPersegiPanjang);
                System.out.println("---------------------------------------");

                System.out.print("Tekan apa saja untuk kembali ke menu utama");
                scanner.next();
                mainMenu();
                break;
            case 0:  mainMenu();
            default:
                System.out.println("\nPilihan Tidak Ada, cobalagi \n");
                bidang();
            }

        }

    public static void volum(){
        //disini tambahin menu pilih volum
        System.out.println("--------------------------------");
        System.out.println("Pilih Bidang Yang Akan dihitung Volum nya");
        System.out.println("--------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke Menu Sebelumnya");
        System.out.print("Pilihan : ");
        int pilihan2 = scanner.nextInt();
        switch (pilihan2) {
            //sesuaikan dengan method bidang
            case 1:
                //rumus kubus
                System.out.println("-----------------------");
                System.out.println("Anda Memilih  Volum Kubus");
                System.out.println("-----------------------");
                System.out.print("Masukkan Sisi : ");
                int sisi = scanner.nextInt();

                System.out.println("\nprocessing\n");
                System.out.println("Volum dari kubus = " + sisi * sisi * sisi);

                System.out.println("---------------------------------------");
                System.out.print("Tekan apa saja untuk kembali ke menu utama");
                scanner.next();
                mainMenu();
                break;
            case 2:
                //rumus bbalok
                System.out.println("-----------------------");
                System.out.println("Anda Memilih Volum Balok");
                System.out.println("-----------------------");

                System.out.print("Masukkan panjang: ");
                int panjangBalok = scanner.nextInt();

                System.out.print("Masukkan lebar: ");
                int lebarBalok = scanner.nextInt();

                System.out.print("Masukkan tinggi: ");
                int tinggiBalok = scanner.nextInt();

                int volumBalok = panjangBalok * lebarBalok * tinggiBalok;
                System.out.println("\nprocessing\n");
                System.out.println("Volum dari balok adalah " + volumBalok);
                System.out.println("---------------------------------------");

                System.out.print("tekan apa saja untuk kembali ke menu utama ");
                scanner.next();
                mainMenu();
            case 3:
                System.out.println("-----------------------");
                System.out.println("Anda Memilih Volum Tabung");
                System.out.println("-----------------------");
                System.out.print("Masukkan jari-jari: ");
                int jariJariTabung = scanner.nextInt();

                System.out.print("Masukkan tinggi: ");
                int tinggiTabung = scanner.nextInt();

                double phi = 3.14;
                double volumTabung = phi * jariJariTabung * jariJariTabung * tinggiTabung;
                System.out.println("\nprocessing\n");
                System.out.println("Volum dari tabung adalah " + volumTabung);
                System.out.println("---------------------------------------");

                System.out.print("tekan apa saja untuk kembali ke menu utama ");
                scanner.next();
                mainMenu();
            case 0: mainMenu();
            default:
                 System.out.println("\nPilihan Tidak Ada, cobalagi \n");
                 volum();
             }
    }
    }
